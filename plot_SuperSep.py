import tables
import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np
import mplhep as hep
hep.style.use(hep.style.CMS)
from scan_info import pp2022, pp2023, pp2024

nBCIDs = 3564
hostPath = '/home/nimmitha/mnts'
# hostPath = '/afs/cern.ch/user/n/nkarunar/mnts'


def get_binomial_error(row):
    step_mean = row['AvgRate']
    # step_error = row['SEM']
    n = row['nNB4']

    # https://github.com/numpy/numpy/issues/11448
    if step_mean >= 0 and step_mean <= 1.0:
        numerator = step_mean*(1-step_mean)
        denominator = 4*4096*n
        step_error_binomial = np.sqrt(numerator/denominator)
    else:
        step_error_binomial = 0
    return step_error_binomial

# per-scan
def std_avg(x: pd.DataFrame, error_type='STD'):
    d = {}
    d['scan_AvgRate'] = x['AvgRate'].mean()
    if error_type == 'SEM':
        # print("Using SEM")
        d['scan_SEM'] = x['AvgRate'].sem()
    elif error_type == 'STD':
        # print("Using Standard Deviation")
        d['scan_STD'] = x['AvgRate'].std()
    return pd.Series(d, index=d.keys())

# average of the ss scans
def simple_avg(x: pd.DataFrame):
    d = {}
    d['Bkg'] = x['scan_AvgRate'].mean()

    # check if a column exists
    if 'scan_SEM' in x.columns:
        d['Err'] = sum(x['scan_SEM']**2)**0.5 / len(x)
    elif 'scan_STD' in x.columns:
        d['Err'] = sum(x['scan_STD']**2)**0.5 / len(x)
    return pd.Series(d, index=d.keys())

def weighted_avg(x: pd.DataFrame):
    d = {}
    if 'scan_SEM' in x.columns:
        weight = x['scan_SEM']**(-2)
    elif 'scan_STD' in x.columns:
        weight = x['scan_STD']**(-2)
    d['Bkg'] = (x['scan_AvgRate'] * weight).sum() / weight.sum()
    d['Err'] = np.sqrt(1 / weight.sum())
    return pd.Series(d, index=d.keys())

def get_table_data(detector, scan, file, time_range):
    start, end = time_range
    # print("  ", file)

    h5file = tables.open_file(file, 'r')

    if 'PLT' in detector:
        if detector == 'PLT':
            hist = h5file.root.scan5_pltlumizero
        else:
            channel = int(detector.strip('PLT'))
            tableName = f'pltlumizero_{channel}'
            hist = h5file.root[tableName]
    elif detector == 'HFET':
        hist = h5file.root.scan5_hfetlumi
    elif detector == 'HFOC':
        hist = h5file.root.scan5_hfoclumi
    elif detector == 'BCM1F':
        hist = h5file.root.scan5_bcm1flumi
    elif 'BCM1FUTCA' in detector:
        if detector == 'BCM1FUTCA':
            hist = h5file.root.scan5_bcm1futcalumi
        else:
            channel = int(detector[9:])
            tableName = f'bcm1futcalumi{channel}'
            if tableName in h5file.root:
                hist = h5file.root[tableName]
            else:
                print(f'{tableName } not available in hd5 file.')
                h5file.close()
                return None
    else:
        raise ValueError('Unknown detector name. No such table in hd5 file.')

    df1 = pd.DataFrame(hist[:][['timestampsec']])
    df2 = pd.DataFrame(hist[:]['bxraw'])

    table = pd.concat([df1, df2], axis=1)
    table = table[(table['timestampsec'] >= start) & (table['timestampsec'] <= end)]

    h5file.close()
    return table

def get_summary_df(do_detectors, do_scans, file_path, scan_details):
    dfs = []
    for detector in do_detectors:
        for scan in do_scans:
            print(f'Processing {detector} {scan}')
            file = os.path.join(file_path, scan_details[scan]['file'])
            time_range = scan_details[scan]['time_range']
            table = get_table_data(detector, scan, file, time_range)

            if table is None:
                continue

            avg_per_bx = table.mean(axis=0).drop('timestampsec')
            SEM_per_bx = table.sem(axis=0).drop('timestampsec')
            STD_per_bx = table.std(axis=0).drop('timestampsec')

            summary_scan = pd.concat([avg_per_bx, SEM_per_bx, STD_per_bx], keys=['AvgRate', 'SEM', 'STD'], axis=1)

            summary_scan = summary_scan.rename_axis('BCID').reset_index()
            summary_scan.insert(0, 'Detector', detector)
            summary_scan.insert(1, 'Scan', scan)
            summary_scan.insert(2, 'nNB4', len(table))
            # print(summary_scan)

            dfs.append(summary_scan)

    results_df = pd.concat(dfs, ignore_index=True)
    results_df['Binomial'] = results_df.apply(get_binomial_error, axis=1)

    return results_df

def make_plots(avail_detectors, do_scans, results, results_scan, results_final, BCIDtype, year):
    # make directories for each BCID group
    os.makedirs(f'{year}/{BCIDtype}', exist_ok=True)

    for detector in avail_detectors:
        plt.figure(figsize=(10, 6))
        results_scan_det = results_scan[results_scan['Detector']==detector]
        det_avg = results_final[results_final['Detector']==detector]['Bkg'].values[0]
        det_err = results_final[results_final['Detector']==detector]['Err'].values[0]

        if 'scan_SEM' in results_scan_det.columns:
            plt.errorbar(results_scan_det['Scan'], results_scan_det['scan_AvgRate'], yerr=results_scan_det['scan_SEM'], fmt='o')
        elif 'scan_STD' in results_scan_det.columns:
            plt.errorbar(results_scan_det['Scan'], results_scan_det['scan_AvgRate'], yerr=results_scan_det['scan_STD'], fmt='o')
            
        plt.axhline(det_avg, color='r', linestyle='--', label=f'{det_avg:.2e} $\\pm$ {det_err:.2e}')
        plt.axhspan(det_avg-det_err, det_avg+det_err, alpha=0.2, color='r')
        plt.ylabel('$\mu$ [<hits> per orbit per bx]')
        plt.xlabel('Scan')
        plt.title(f'{detector} SuperSeparation Background - {BCIDtype} BCIDs', fontsize=18, y=1.05)
        plt.legend(fontsize=14)
        plt.tight_layout()
        plt.savefig(f'{year}/{BCIDtype}/Summary_{detector}.png')
        plt.close()

    for detector in avail_detectors:
        ax = plt.gca()
        figure = plt.gcf()
        figure.set_size_inches(15, 6)
        for scan in do_scans:
            color = next(ax._get_lines.prop_cycler)['color']
            df = results[(results['Detector'] == detector) & (results['Scan'] == scan)]
            plt.errorbar(df['BCID'], df['AvgRate'], yerr=df['Binomial'], color=color, fmt='o', label=f'{scan}')
            # horizontal line to show mean
            # plt.axhline(y=df['AvgRate'].mean(), color=color, linestyle='--')
            # plt.errorbar(df['BCID'], df['AvgRate'], fmt='.', label=f'{scan}')

        plt.xlabel('BCID')
        plt.ylabel('$\mu$ [<hits> per orbit per bx]')
        plt.title(f'{detector} SuperSeparation Background  - {BCIDtype} BCIDs', fontsize=18, y=1.05)
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 0.15), ncol=5)
        plt.tight_layout()
        plt.savefig(f'{year}/{BCIDtype}/BCIDs_{detector}.png')
        plt.close()


    # Histograms
    for detector in avail_detectors:
        plt.figure(figsize=(8, 8))
        for scan in do_scans:
            df = results[(results['Detector'] == detector) & (results['Scan'] == scan)]
            plt.hist(df['AvgRate'], bins=10, label=f'{scan}', alpha=0.7)

        plt.xlabel('$\mu$ [<hits> per orbit per bx]', ha='center')
        plt.title(f'{detector} SuperSeparation Background - {BCIDtype} BCIDs', fontsize=18, y=1.05)
        plt.legend()
        plt.tight_layout()
        plt.savefig(f'{year}/{BCIDtype}/Hist_{detector}_AllScans.png')
        plt.close()


def main(do_campaign, do_detectors, do_scans):
    year = do_campaign['year']
    file_path = hostPath + do_campaign['fill_path']

    # print(file_path, hostPath, do_campaign['fill_path'])
    scan_details = do_campaign['scan_details']

    os.makedirs(str(year), exist_ok=True)

    # check if scans are a subset of the keys in scan_details
    if not set(do_scans).issubset(set(scan_details.keys())):
        raise ValueError('scans must be a subset of the keys in scan_details')
    
    # results_df = get_summary_df(do_detectors, do_scans, file_path, scan_details)
    # results_df.to_csv(f'{year}_SS_detailed.csv', index=False)
    results_df = pd.read_csv(f'{year}_SS_detailed.csv')
    print(results_df)

    avail_detectors = results_df['Detector'].unique()
    # print(f'Available detectors: {avail_detectors}')

    # Lets do the calculation for the super separation

    # 0 start BCIDs
    collidingBCID = do_campaign['collidingBCIDs']
    nonCollidingBCID = do_campaign['nonCollidingBCIDs']
    emptyBCID = [i for i in range(nBCIDs) if i not in collidingBCID and i not in nonCollidingBCID]

    BCIDtypes = ['All', 'Colliding', 'NonColliding', 'Empty']
    # BCIDtypes = ['Colliding']

    for BCIDtype in BCIDtypes:
        if BCIDtype == 'All':
            results = results_df
        elif BCIDtype == 'Colliding':
            results = results_df[results_df['BCID'].isin(collidingBCID)]
        elif BCIDtype == 'NonColliding':
            results = results_df[results_df['BCID'].isin(nonCollidingBCID)]
        elif BCIDtype == 'Empty':
            results = results_df[results_df['BCID'].isin(emptyBCID)]
        else:
            raise ValueError('Unknown BCID group')

        results_scan = results.groupby(['Detector', 'Scan']).apply(std_avg).reset_index()
        # results_final = results_scan.groupby(['Detector']).apply(weighted_avg).reset_index()
        results_final = results_scan.groupby(['Detector']).apply(simple_avg).reset_index()

        results_final.to_csv(f'{year}/{BCIDtype}_SS_summary.csv', index=False)

        print(f'{BCIDtype} BCIDs')
        print(results_final)

        make_plots(avail_detectors, do_scans, results, results_scan, results_final, BCIDtype, year)


def make_bgfbun_file(do_campaign):
    plt_channels = [f'PLT{i}' for i in range(16)]
    bcm1f_channels = [f'BCM1FUTCA{i}' for i in range(44)]

    year = do_campaign['year']

    results = pd.read_csv(f'{year}_SS_detailed.csv')
    results = results[~results['Detector'].isin(plt_channels + bcm1f_channels)]
    results = results[results['BCID'].isin(do_campaign['collidingBCIDs'])]
    print(results)

    results_scan = results.groupby(['Detector', 'Scan']).apply(std_avg, include_groups=False).reset_index()
    results_final = results_scan.groupby(['Detector']).apply(simple_avg, include_groups=False).reset_index()

    print(results_final)

    results['BCID'] = results['BCID'].astype(str)
    bgfbun = results.groupby(['Detector', 'BCID']).apply(std_avg, error_type='SEM', include_groups=False).reset_index()

    print(bgfbun)

    table_name_map = {"BCM1F": 'bcm1flumi', "BCM1FUTCA": 'bcm1futcalumi', "HFET": 'hfetlumi', "HFOC": 'hfoclumi', "PLT": 'pltlumizero'}

    detector_dict = {}
    json_data = {}
    fill = do_campaign['fill']

    for detector in bgfbun['Detector'].unique():
        print("Processing ", detector)
        detector_results = bgfbun[bgfbun['Detector'] == detector]
        print(detector_results)
        dict_rates  = dict(zip(detector_results['BCID'], detector_results['scan_AvgRate']))
        dict_rates['NOBCID'] = results_final[results_final['Detector']==detector]['Bkg'].values[0]

        dict_errs   = dict(zip(detector_results['BCID'], detector_results['scan_SEM']))
        dict_errs['NOBCID'] = results_final[results_final['Detector']==detector]['Err'].values[0]

        detector_dict['scan5_' + table_name_map[detector]] = [dict_rates, dict_errs]
        json_data[fill] = detector_dict

    # write to json file
    import json
    with open(f'{year}_SS_bun.json', 'w') as f:
        json.dump(json_data, f, indent=4, sort_keys=True) 


if __name__ == '__main__':
    # Things to use
    do_campaign = pp2024
    do_detectors = []
    do_detectors = do_detectors + ['HFET', 'HFOC', 'PLT', 'BCM1F', 'BCM1FUTCA'] 
    do_detectors = do_detectors + [f'PLT{i}' for i in range(16)]
    do_detectors = do_detectors + [f'BCM1FUTCA{i}' for i in range(40)]
    
    do_scans = ['ss1', 'ss2', 'ss3', 'ss4', 'ss5']
    # do_scans = ['ss1', 'ss2']
    
    # main(do_campaign, do_detectors, do_scans)
    make_bgfbun_file(do_campaign)

